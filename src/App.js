import React from 'react';
import './App.css';
import './style.css';
import Board from './components/Board';

class App extends React.Component {
  render() {
    return (
      <div className="game">
        <div className="game-board">
          <Board />
        </div>
        
      </div>
    );
  }
}

export default App;
